require 'test/unit'
require '3'

class ThreeTest < Test::Unit::TestCase

  def setup
    @three = Three.new
  end

  def test_number_divides_evenly
    assert @three.number_divides_evenly(12, 3)
    assert @three.number_divides_evenly(12, 2)
    assert !@three.number_divides_evenly(12, 5)
    assert @three.number_divides_evenly(13195, 29)
    assert !@three.number_divides_evenly(13195, 30)
  end

  def test_is_prime_returns_true_or_false
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 103, 107, 109, 113].each do |num|
      assert @three.is_prime(num), "failed on #{num}"
    end
  end

  def test_given_large_number_find_the_largest_prime_factor
    assert_equal 29, @three.largest_prime_factor(13195)
  end

  def test_sieve
    list = [*2..30]
    res  = [2,3,5,7,11,13,17,19,23,29]
    assert_equal res, @three.sieve(list[0], list)
  end

  def test_multiple_of_p
    assert @three.multiple_of_p(4, 2)
    assert @three.multiple_of_p(6, 2)
    assert @three.multiple_of_p(12, 2)
    assert !@three.multiple_of_p(5, 2)
    assert @three.multiple_of_p(6, 3)
  end

  def test_next_p
    list = [2,3,5,7,11,13,17,19,23,29]
    p = 7
    assert_equal 11, @three.find_next_p(p, list)
  end

  def test_answer_to_problem_three
    x = 600851475143
    t1 = Time.now
    puts @three.largest_prime_factor(x)
    t2 = Time.now
    puts t2 - t1
  end

end


