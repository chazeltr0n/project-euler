# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

class Three

  def initialize
  end

  def number_divides_evenly(x, y)
    x % y == 0
  end

  def is_prime(n)
    # uses the sieve of eratosthenes
    list = [*2..n]
    sieve(2, list).include?(n)
  end

  def largest_prime_factor(num)
    res = nil
    (num - 1).downto(0) do |n|
      if number_divides_evenly(num, n) && is_prime(n)
        res = n
        break
      end
    end
    res
  end

  def multiple_of_p(x, p)
    return false if x <= p
    x % p == 0
  end

  def find_next_p(p, list)
    p_index = list.index(p)
    list[p_index + 1]
  end

  def sieve(p, list)
    new_list = list.select{|x| x if !multiple_of_p(x, p) }
    next_p = find_next_p(p, new_list)
    if next_p.nil? || (next_p > list.last)
      return new_list
    else
      sieve(next_p, new_list)
    end
  end

end


