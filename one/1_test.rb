require 'test/unit'
require '1'

class NaturalNumbersTest < Test::Unit::TestCase

  def setup
    @natural_numbers = NaturalNumbers.new
  end

  def test_list_natural_numbers_returns_array
    assert @natural_numbers.list_natural_numbers_below(10).is_a?(Array)
  end

  def test_list_natural_numbers_below_ten_returns_correct_results
    assert_equal @natural_numbers.list_natural_numbers_below(10), [1,2,3,4,5,6,7,8,9]
  end

  def test_number_is_multiple_of_three_or_five
    nums = @natural_numbers.list_natural_numbers_below(10)
    assert_equal [3,5,6,9], @natural_numbers.list_multiples_of_three_or_five(nums)
  end

  def test_multiple_of_three_or_five_returns_just_that
    assert @natural_numbers.multiple_of_three_or_five(5)
    assert !@natural_numbers.multiple_of_three_or_five(17)
    assert @natural_numbers.multiple_of_three_or_five(100)
    assert @natural_numbers.multiple_of_three_or_five(45)
    assert !@natural_numbers.multiple_of_three_or_five(2)
  end

  def test_sum_multiples
    nums = @natural_numbers.list_natural_numbers_below(10)
    shorter_list = @natural_numbers.list_multiples_of_three_or_five(nums)
    assert_equal 23, @natural_numbers.sum_list(shorter_list)
  end

  def test_answer_to_problem_one
    puts "THE ANSWER IS: #{@natural_numbers.give_me_an_answer.inspect}"
  end

end
