#include <stdio.h>

long
pal(int i, int j)
{
	char	*f, *r;
	char	fwd[64];

	sprintf(fwd, "%ld", i * j);
	f = fwd;
	r = fwd + strlen(fwd) - 1;
	for (; f <= r; f++, r--) {
		if (*f != *r)
			break;
		if (f == r)
			break;
	}
	if (f < r)
		return 0;
	return i * j;
}

int
do_diag(int i, int j)
{
	int res;

	while (i <= 999) {
		if ((res = pal(i, j))) {
			printf("%d * %d = ", i, j);
			return res;
		}
		i++;
		j--;
	}
	return 0;
}

int
main(int argc, char **argv)
{
	int i, j, res;

	i = 1000;
	while (--i > 99) {
		j = i;
		if ((res = do_diag(i, j))) {
			printf("%ld\n", res);
			return 0;
		}
		if ((res = do_diag(i, j-1))) {
			printf("%ld\n", res);
			return 0;
		}
	}
	puts("0");
	return 0;
}

