#include <math.h>
#include <string.h>

/* hints to compile...
 * cc -o prime -Wall prime.c -lm
 */

int
is_prime(int number)
{
      int i;
      int is_prime = 1;

      for(i = 2; i < number; i++) {
	    if(fmod(number, i) == 0) {
		  is_prime = 0;
		  break;
	    }
      }
      return(is_prime);
}

int
is_odd(number)
{
      return(number % 2);
}

int
is_even(number)
{
      return(!(number % 2));
}

int
choose_random(number, minimum)
{
      /* return 2 .. n - 2 */
      int r;

      r = rand() % (number - 1);

      if (r < minimum) r = minimum;

      return(r);
}

int
power_of(num, power)
{
      int i;
      int res;
      
      res = 1;

      for(i = 1; i <= power; i++) {
	    res *= num;
      }
      return(res);
}

int
is_prime_float(double number)
{
      int i;
      int is_prime = 1;

      if(number == 2.0) return(0);

      for(i = 2; i < number; i++) {
	    if(fmod(number, i) == 0.0) {
		  is_prime = 0;
		  break;
	    }
      }
      return(is_prime);
}

char *
reverse_string(char *s, char *rs)
{
      int i, j;
      char tmp;

      strncpy(rs, s, 10);

      for(i = 0, j = strlen(rs) - 1; i < j; i++, j--) {
	    tmp = rs[i];
	    rs[i] = rs[j];
	    rs[j] = tmp;
      }

      return(rs);
}

int
search_score(char *term, char *text)
{
      char *text_ptr;
      int score       = 0;
      int text_length = strlen(text);
      int term_length = strlen(term);

      int i,j;

      text_ptr = text;
      
      for (i = 0; i < (text_length - 1); i++) {
	    if ((strncasecmp(term, text_ptr, term_length)) == 0) {
		  score++;
		  for (j = 0; j < (term_length - 1); j++) text_ptr++;
	    } else {
		  text_ptr++;
	    }
      }

      return(score);
}
