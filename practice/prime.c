#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*  cc -o prime -Wall prime.c -lm */

int
is_prime(int number)
{
      int i;
      int is_prime = 1;

      for(i = 2; i < number; i++) {
	    if(fmod(number, i) == 0) {
		  is_prime = 0;
		  break;
	    }
      }
      return(is_prime);
}

int
main(int argc, char *argv[])
{

      int number = 3;
      int count = 1;
      while (1) {
	    if (is_prime(number)) {
		  printf("%i\t", number);
		  count++;
		  if (count == 10001) break;
	    }
	    number++;
      }

      printf("answer: %i\n", number);

      return(0);

}
