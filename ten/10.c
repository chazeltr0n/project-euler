/* problem #10
 * projecteuler.net
 * calculate the sum of all the primes below two million
 */
#include <stdio.h>
#include <stdlib.h>
#include "../include/euler.h"

int
main(int argc, char * argv[])
{
	int max             = 2000000;
	int number          = 3;
	long long prime_sum = 2;

	while (1) {
		if (is_even(number)) { 
			number++;
			continue;
		}

	    	if (is_prime(number)) {
			prime_sum += number;
		}

//printf("debug %i | %lld\n", number, prime_sum);

		number++;

	    	if (number >= max) {
			break;
		}
      	}

      	printf("The sum of all primes below %i is %lld\n", max, prime_sum);

      	return(0);
}
