
UP_TO = 20
num   = 1
ok    = false

while(!ok) do

  scn = 0

  (1..UP_TO).each do |x|
    if num % x == 0
      scn += 1
    else
      break
    end
  end

  if scn == UP_TO
    ok = true
    puts "ANSWER: #{num}"
  else
    num += 1
  end

end
